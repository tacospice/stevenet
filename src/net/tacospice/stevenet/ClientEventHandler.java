package net.tacospice.stevenet;

public interface ClientEventHandler
{
	public boolean nodeCreated(int nodeId, int classId, boolean owner);

	public void nodeDestroyed(int nodeId);

	public void logMessage(String message);

	/**
	 * Fires when another user has connected to the server
	 * 
	 * @param name
	 */
	public void userConnected(String name);

	/**
	 * Fires when another user has disconnected from the server
	 * 
	 * @param name
	 */
	public void userDisconnected(String name);

	/**
	 * Fires when the local client has connected to a server
	 * 
	 * @param name
	 */
	public void localConnected(String name);

	/**
	 * Fires before the local client is disconnected from a server
	 * 
	 * @param reason
	 */
	public void localDisconnected(NetworkMessages.Disconnect.Reason reason);

	/**
	 * Fires when the client connection is closed
	 */
	public void connectionClosed();

	/**
	 * Fires when an unrecognized message is received
	 * 
	 * @param message
	 */
	public void unknownMessage(Object message);
}