package net.tacospice.stevenet.gnl;

import java.util.ArrayList;

import net.tacospice.stevenet.Replicator;
import net.tacospice.stevenet.ReplicatorMessage;

public final class StateReplicator extends Replicator
{
	static public class State
	{
		public int sequenceNumber = 0;
		public float position[] = { 0, 0 };
		public float velocity[] = { 0, 0 };

		public void copy(State other)
		{
			position[0] = other.position[0];
			position[1] = other.position[1];
			velocity[0] = other.velocity[0];
			velocity[1] = other.velocity[1];
			sequenceNumber = other.sequenceNumber;
		}
	}

	public final static int maxStates = 10;
	private final ArrayList<State> states;

	private int nextSequenceNumber = 0;

	public StateReplicator(ReplicationType replicationType, boolean ownerToServer)
	{
		super(replicationType, ownerToServer, true);

		states = new ArrayList<StateReplicator.State>(maxStates);
		states.add(new State());
	}

	public ArrayList<State> getStates()
	{
		return states;
	}

	private void addState(State state)
	{
		synchronized (states)
		{
			if (states.size() == maxStates)
				states.remove(0);
			states.add(state);
		}
	}

	public void setPosition(float x, float y)
	{
		getMostRecent().position[0] = x;
		getMostRecent().position[1] = y;
	}

	public void setVelocity(float x, float y)
	{
		getMostRecent().velocity[0] = x;
		getMostRecent().velocity[1] = y;
	}

	public float getX()
	{
		return getMostRecent().position[0];
	}

	public float getY()
	{
		return getMostRecent().position[1];
	}

	public float getVelocityX()
	{
		return getMostRecent().velocity[0];
	}

	public float getVelocityY()
	{
		return getMostRecent().velocity[1];
	}

	public State getOldest()
	{
		synchronized (states)
		{
			return states.get(0);
		}
	}

	public State getMostRecent()
	{
		synchronized (states)
		{
			return states.get(states.size() - 1);
		}
	}

	public State getNextMostRecent()
	{
		synchronized (states)
		{
			return states.get(states.size() - 2);
		}
	}

	/*
	 * Replicator's network message class
	 */
	static public class Message extends ReplicatorMessage
	{
		public int sequenceNumber;
		public float x;
		public float y;
		public float vx;
		public float vy;
	}

	@Override
	public void updateFromMessage(ReplicatorMessage message)
	{
		if (message instanceof Message)
		{
			State oldState = new State();
			oldState.copy(getMostRecent());
			addState(oldState);

			getMostRecent().sequenceNumber = ((Message) message).sequenceNumber;

			getMostRecent().position[0] = ((Message) message).x;
			getMostRecent().position[1] = ((Message) message).y;
			getMostRecent().velocity[0] = ((Message) message).vx;
			getMostRecent().velocity[1] = ((Message) message).vy;
		}
	}

	@Override
	public ReplicatorMessage getMessage(int nodeId, int replicatorId)
	{
		Message m = new Message();

		m.sequenceNumber = nextSequenceNumber++;
		m.x = getMostRecent().position[0];
		m.y = getMostRecent().position[1];
		m.vx = getMostRecent().velocity[0];
		m.vy = getMostRecent().velocity[1];

		m.nodeId = nodeId;
		m.replicatorId = replicatorId;

		return m;
	}

}
