package net.tacospice.stevenet.gnl;

import java.io.IOException;

import net.tacospice.stevenet.ClientEventHandler;
import net.tacospice.stevenet.NetClient;
import net.tacospice.stevenet.NetworkMessages.Disconnect.Reason;
import net.tacospice.stevenet.Node;

public class GameClient implements ClientEventHandler
{
	protected final NetClient client;
	protected final GameEventHandler eventHandler;
	protected final Game game;

	public GameClient(Game game, GameEventHandler handler)
	{
		client = new NetClient();
		client.setEventHandler(this);
		client.setUpdateRate(20);

		eventHandler = handler;

		this.game = game;

		game.registerMessages(client.getKryoClient());
		game.registerClasses(client);
	}

	public void connect(String playerName, String host, int tcpPort, int udpPort, int timeout) throws IOException
	{
		client.connect(playerName, host, tcpPort, udpPort, timeout);
	}

	public void stop()
	{
		client.close();
		game.newGame();
	}

	public void sendChat(String message)
	{
		client.sendChat(message);
	}

	public void update(float delta)
	{
		client.update(delta);
		game.update(delta);
	}

	@Override
	public boolean nodeCreated(int nodeId, int classId, boolean owner)
	{
		Node n = new Node(nodeId, classId, false);

		if (classId == Entity.getClassId())
		{
			Entity entity = new Entity(n, 0, 0);
			if (owner)
				n.setOwner(1);

			game.addEntity(entity);
			eventHandler.entityCreated(entity, false);

			client.addNode(n);

			return true;
		}

		return false;
	}

	@Override
	public void nodeDestroyed(int nodeId)
	{
		Entity e = game.getEntityByNodeId(nodeId);

		if (e != null)
		{
			game.removeEntity(e);
			eventHandler.entityRemoved(e, false);
		}
	}

	@Override
	public void logMessage(String message)
	{
		eventHandler.logMessage(message, false);
	}

	@Override
	public void userConnected(String name)
	{
		logMessage("> " + name + " connected");
	}

	@Override
	public void userDisconnected(String name)
	{
		logMessage("> " + name + " disconnected");
	}

	@Override
	public void localConnected(String name)
	{
	}

	@Override
	public void localDisconnected(Reason reason)
	{
		logMessage("> disconnected from server: " + reason.toString());
	}

	@Override
	public void connectionClosed()
	{
		logMessage("> Server connection closed");
	}

	@Override
	public void unknownMessage(Object message)
	{
		logMessage("> Unknown message: " + message.getClass().toString());
	}
}
