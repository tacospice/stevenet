package net.tacospice.stevenet.gnl;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import net.tacospice.stevenet.NetConnection;
import net.tacospice.stevenet.NetServer;
import net.tacospice.stevenet.Node;
import net.tacospice.stevenet.ServerEventHandler;

public class GameServer implements ServerEventHandler
{
	public final NetServer server;
	protected final GameEventHandler eventHandler;
	protected final Game game;

	public GameServer(Game game, GameEventHandler handler)
	{
		server = new NetServer();
		server.setEventHandler(this);

		eventHandler = handler;

		this.game = game;

		logMessage("* Registering server messages ..");
		game.registerMessages(server.getKryoServer());
		logMessage("* Registering game classes ..");
		game.registerClasses(server);
	}

	public void startListen(int tcpPort, int udpPort) throws IOException
	{
		logMessage("* Opening server on ports tcp " + tcpPort + "/udp " + udpPort + " ..");
		server.listen(tcpPort, udpPort);
		logMessage("  Listening");
	}

	public void stop()
	{
		server.close();
		game.newGame();
	}

	public void update(float delta)
	{
		server.update(delta);
		game.update(delta);
	}

	@Override
	public void logMessage(String message)
	{
		eventHandler.logMessage(message, true);
	}

	@Override
	public void clientConnected(NetConnection connection)
	{
		logMessage("* Connected client " + connection.getID() + ", " + connection.getRemoteAddressTCP());
		Node entityNode = server.createNode(Entity.getClassId());
		entityNode.setOwner(connection.getID());

		Entity entity = new Entity(entityNode, 0, 0);

		game.addEntity(entity);
		server.addNode(entityNode);
		eventHandler.entityCreated(entity, true);

		logMessage("  Created entity " + entityNode.getId() + "/" + entityNode.getClassId());
	}

	@Override
	public void clientDisconnected(NetConnection connection)
	{
		logMessage("* Disconnected client " + connection.getID() + ", " + connection.getRemoteAddressTCP());

		List<Node> clientNodes = server.getNodesByOwner(connection.getID());

		Iterator<Node> nodeIterator = clientNodes.iterator();

		// For each node owned by connection
		Entity e;
		while (nodeIterator.hasNext())
		{
			e = game.getEntityByNodeId(nodeIterator.next().getId());

			if (e != null)
			{
				game.removeEntity(e);
				eventHandler.entityRemoved(e, true);
			}
		}

		logMessage("  Removed " + clientNodes.size() + " entities");
	}

	@Override
	public void unknownMessage(Object message)
	{
		eventHandler.logMessage("* Unknown message: " + message.getClass().toString(), true);
	}
}
