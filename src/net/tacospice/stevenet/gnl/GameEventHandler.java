package net.tacospice.stevenet.gnl;

public interface GameEventHandler
{
	public void logMessage(String message, boolean serverSide);

	public void entityCreated(Entity entity, boolean serverSide);

	public void entityRemoved(Entity entity, boolean serverSide);
}
