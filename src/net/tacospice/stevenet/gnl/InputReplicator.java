package net.tacospice.stevenet.gnl;

import java.util.ArrayList;

import net.tacospice.stevenet.Replicator;
import net.tacospice.stevenet.ReplicatorMessage;

public final class InputReplicator extends Replicator
{
	static public class Input
	{
		public int sequenceNumber = 0;
		public Float pad[] = { 0.0f, 0.0f };
	}

	public final static int maxInputs = 200;
	private final ArrayList<Input> inputs;

	private int nextSequenceNumber = 0;

	private int lastSentInput = -1;

	public InputReplicator(ReplicationType replicationType, boolean ownerToServer)
	{
		super(replicationType, ownerToServer, true);

		inputs = new ArrayList<InputReplicator.Input>(maxInputs);
		inputs.add(new Input());
	}

	public ArrayList<Input> getInputs()
	{
		return inputs;
	}

	private void addInput(Input input)
	{
		synchronized (inputs)
		{
			if (inputs.size() == maxInputs)
				inputs.remove(0);
			inputs.add(input);
		}
	}

	private void addInput(float x, float y, int sequence)
	{
		Input i = new Input();
		i.pad[0] = x;
		i.pad[1] = y;
		i.sequenceNumber = sequence;

		addInput(i);
	}

	public void addInput(float x, float y)
	{
		addInput(x, y, nextSequenceNumber++);
	}

	/*
	 * Replicator's network message class
	 */
	static public class Message extends ReplicatorMessage
	{
		public Input[] inputs;
	}

	/*
	 * For this class, updateFromMessage is server-side only
	 */
	@Override
	public void updateFromMessage(ReplicatorMessage message)
	{
		if (message instanceof Message)
		{
			Message rm = (Message) message;

			for (Input i : rm.inputs)
				addInput(i.pad[0], i.pad[1], i.sequenceNumber);
		}
	}

	/*	
	 * Client-side only
	 */
	@Override
	public ReplicatorMessage getMessage(int nodeId, int replicatorId)
	{
		Message m = new Message();
		ArrayList<Input> toSend = new ArrayList<InputReplicator.Input>();

		synchronized (inputs)
		{
			for (Input i : inputs)
			{
				if (i.sequenceNumber >= lastSentInput)
					toSend.add(i);
			}
		}

		if (toSend.size() > 0)
			lastSentInput = toSend.get(toSend.size() - 1).sequenceNumber;

		m.inputs = new Input[toSend.size()];
		m.inputs = toSend.toArray(m.inputs);
		m.nodeId = nodeId;
		m.replicatorId = replicatorId;

		return m;
	}
}
