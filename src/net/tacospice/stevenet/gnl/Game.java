package net.tacospice.stevenet.gnl;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import net.tacospice.stevenet.ClientServer;

import com.esotericsoftware.kryonet.EndPoint;

public class Game
{
	private float accumulator;
	private float timeStep;
	private final CopyOnWriteArrayList<Entity> entities;

	public Game()
	{
		accumulator = 0.0f;
		setTimestep(60);

		entities = new CopyOnWriteArrayList<Entity>();
	}

	public void newGame()
	{
		entities.clear();
	}

	/*
	 * Registers the game-specific classes for serialization
	 */
	protected void registerClasses(ClientServer cs)
	{
		Entity.register(cs);
	}

	/*
	 * Registers our custom network messages
	 */
	protected void registerMessages(EndPoint ep)
	{
		ep.getKryo().register(StateReplicator.Message.class);

		ep.getKryo().register(Float[].class);
		ep.getKryo().register(InputReplicator.Input.class);
		ep.getKryo().register(InputReplicator.Input[].class);
		ep.getKryo().register(InputReplicator.Message.class);
	}

	public void addEntity(Entity entity)
	{
		entities.add(entity);
	}

	public void removeEntity(Entity entity)
	{
		entities.remove(entity);
	}

	public void update(float delta)
	{
		accumulator += delta;

		Iterator<Entity> entityIterator;
		Entity entity;

		while (accumulator >= timeStep)
		{
			accumulator -= timeStep;

			entityIterator = entities.iterator();
			while (entityIterator.hasNext())
			{
				entity = entityIterator.next();

				entity.update(timeStep);
			}
		}
	}

	/*
	 * Getters, setters
	 */
	public void setTimestep(float fps)
	{
		timeStep = 1 / fps;
	}

	protected Entity getEntityByNodeId(int nodeId)
	{
		Iterator<Entity> entityIterator;
		Entity entity;

		entityIterator = entities.iterator();
		while (entityIterator.hasNext())
		{
			entity = entityIterator.next();

			if (entity.getNode().getId() == nodeId)
				return entity;
		}

		return null;
	}
}
