package net.tacospice.stevenet.gnl;

import java.util.ArrayList;

import net.tacospice.stevenet.ClientServer;
import net.tacospice.stevenet.Node;
import net.tacospice.stevenet.Replicator.ReplicationType;
import net.tacospice.stevenet.gnl.InputReplicator.Input;
import net.tacospice.stevenet.gnl.StateReplicator.State;

/**
 * A networked game entity
 * 
 * @author e
 * 
 */
public class Entity
{
	/*
	 * Server/client class registration
	 */
	static private int classId = -1;

	static public void register(ClientServer cs)
	{
		classId = cs.registerClass(Entity.class);
	}

	static public int getClassId()
	{
		return classId;
	}

	/*
	 * Fields
	 */
	private final Node node;
	protected final InputReplicator input;
	protected final StateReplicator stateRep;
	private final StateReplicator.State state;
	private final StateReplicator.State next;
	private final StateReplicator.State recent;

	private int nextX;
	private int nextY;

	private int lastAppliedInput = -1;

	private float proxyAccumulator = 0.0f;

	/*
	 * Public methods
	 */
	public Entity(Node node, float x, float y)
	{
		this.node = node;
		state = new StateReplicator.State();
		next = new StateReplicator.State();
		recent = new StateReplicator.State();

		nextX = nextY = 0;

		node.beginReplicationSetup();
		{
			// Position, server -> all
			stateRep = (StateReplicator) node.addReplicator(new StateReplicator(ReplicationType.ToBoth, false));
			// Input, owner -> server
			input = (InputReplicator) node.addReplicator(new InputReplicator(ReplicationType.ToNeither, true));
			// Let subclasses perform any additional setup
			doCustomReplicationSetup(node);
		}
		node.endReplicationSetup();

		stateRep.setPosition(x, y);
	}

	protected void doCustomReplicationSetup(Node node)
	{

	}

	public void update(float delta)
	{
		// Perform role-specific logic
		if (node.isServerSide())
			updateServer(delta);
		else if (node.getOwner() == 1)
			updateOwner(delta);
		else
			updateProxy(delta);
	}

	protected void updateServer(float delta)
	{
		ArrayList<Input> inputs = input.getInputs();

		synchronized (inputs)
		{
			for (Input i : inputs)
			{
				if (i.sequenceNumber <= lastAppliedInput)
					continue;

				stateRep.setVelocity(i.pad[0] * 128, i.pad[1] * 128);
				stateRep.setPosition(stateRep.getX() + stateRep.getVelocityX() * delta, stateRep.getY() + stateRep.getVelocityY() * delta);

				lastAppliedInput = i.sequenceNumber;

				// TODO: Notify client of input # -> result, remove buffered input already applied

				break;
			}
		}
	}

	protected void updateOwner(float delta)
	{
		// Store input
		input.addInput(nextX, nextY);

		// Apply input
		state.velocity[0] = nextX * 128;
		state.velocity[1] = nextY * 128;
		state.position[0] += state.velocity[0] * delta;
		state.position[1] += state.velocity[1] * delta;

		// TODO: Store input -> result to be compared with notification from server, remove buffered input OK'd by server
	}

	float lerp(float a, float b, float f)
	{
		return a + f * (b - a);
	}

	protected void updateProxy(float delta)
	{
		if (stateRep.getStates().size() < 2)
			return;

		proxyAccumulator += delta;

		if (proxyAccumulator >= 0.1f)
		{
			proxyAccumulator = proxyAccumulator % 0.1f;
			next.copy(stateRep.getNextMostRecent());
			recent.copy(stateRep.getMostRecent());
		}

		float ratio = proxyAccumulator / 0.1f;

		state.position[0] = lerp(next.position[0], recent.position[0], ratio);
		state.position[1] = lerp(next.position[1], recent.position[1], ratio);
	}

	/*
	 * Getters, setters
	 */
	public float getPositionX(boolean server)
	{
		if (server)
			return stateRep.getX();
		else
			return state.position[0];
	}

	public float getPositionY(boolean server)
	{
		if (server)
			return stateRep.getY();
		else
			return state.position[1];
	}

	public float getVelocityX(boolean server)
	{
		if (server)
			return stateRep.getVelocityX();
		else
			return state.velocity[0];
	}

	public float getVelocityY(boolean server)
	{
		if (server)
			return stateRep.getVelocityY();
		else
			return state.velocity[1];
	}

	public void setPosition(float x, float y)
	{
		stateRep.setPosition(x, y);
	}

	public void setVelocity(float x, float y)
	{
		stateRep.setVelocity(x, y);
	}

	public void setInput(int x, int y)
	{
		if (x > 0)
			nextX = 1;
		else if (x < 0)
			nextX = -1;
		else
			nextX = 0;

		if (y > 0)
			nextY = 1;
		else if (y < 0)
			nextY = -1;
		else
			nextY = 0;
	}

	public Node getNode()
	{
		return node;
	}

	public ArrayList<State> getStates()
	{
		return stateRep.getStates();
	}
}
