package net.tacospice.stevenet;

import java.util.ArrayList;
import java.util.Iterator;

public final class Node
{
	private final int id;

	private int ownerId;
	private final int classId;

	private final boolean serverSide;

	private boolean settingUp;
	private boolean setupDone;

	private boolean wasBroadcast;

	private final ArrayList<Replicator> replicators;

	/**
	 * @param id
	 *            The id to assign to this node
	 */
	public Node(int id, int classId, boolean serverSide)
	{
		this.id = id;
		ownerId = -1;
		this.classId = classId;

		this.serverSide = serverSide;
		settingUp = setupDone = false;

		wasBroadcast = false;

		replicators = new ArrayList<Replicator>();
	}

	/**
	 * Returns this node's id
	 * 
	 * @return This node's id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * Begin the replication setup process
	 */
	public void beginReplicationSetup()
	{
		if (setupDone)
			throw new IllegalStateException("Node setup has already been performed");
		if (settingUp)
			throw new IllegalStateException("Node is already in setup state");

		settingUp = true;
	}

	public Replicator addReplicator(Replicator rep)
	{
		if (setupDone)
			throw new IllegalStateException("Node setup has already been performed");
		if (!settingUp)
			throw new IllegalStateException("Node is not in setup state");

		replicators.add(rep);

		return rep;
	}

	/**
	 * Finish the replication setup process
	 */
	public void endReplicationSetup()
	{
		if (!settingUp)
			throw new IllegalStateException("Node is not in setup state");

		settingUp = false;
		setupDone = true;
	}

	/**
	 * Check whether the replication setup process is done or not
	 * 
	 * @return True if the replication setup process is done, otherwise false
	 */
	public boolean getSetupDone()
	{
		return setupDone;
	}

	/**
	 * Get an iterator to the replicators collection
	 * 
	 * @return An iterator to the replicators collection
	 */
	public Iterator<Replicator> getIterator()
	{
		return replicators.iterator();
	}

	/**
	 * Retrieve the replicator at the specified index.
	 * 
	 * @param index
	 *            The index of the replicator to retrieve.
	 * @return The requested replicator if it exists, otherwise null.
	 */
	public Replicator getReplicator(int index)
	{
		if (index >= replicators.size())
			return null;

		return replicators.get(index);
	}

	/**
	 * Retrieve the replicator at the specified index, cast to the supplied type.
	 * 
	 * @param index
	 *            The index of the replicator to retrieve.
	 * @param type
	 *            The type to attempt casting the retrieved replicator to.
	 * @return The requested replicator, cast to the supplied type, or null if the index is invalid or type incompatible.
	 */
	public <T extends Replicator> T getReplicator(int index, Class<T> type)
	{
		if (index >= replicators.size())
			return null;

		Replicator r = replicators.get(index);

		if (!type.isInstance(r))
			return null;

		return type.cast(r);
	}

	public int getOwner()
	{
		return ownerId;
	}

	public void setOwner(int id)
	{
		this.ownerId = id;
	}

	public int getClassId()
	{
		return classId;
	}

	public boolean isServerSide()
	{
		return serverSide;
	}

	public boolean wasBroadcast()
	{
		return wasBroadcast;
	}

	public void broadcastDone()
	{
		wasBroadcast = true;
	}
}
