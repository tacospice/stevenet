package net.tacospice.stevenet;

import java.io.IOException;

import net.tacospice.stevenet.NetworkMessages.ChatMessage;
import net.tacospice.stevenet.NetworkMessages.CreateNode;
import net.tacospice.stevenet.NetworkMessages.DestroyNode;
import net.tacospice.stevenet.NetworkMessages.Disconnect;
import net.tacospice.stevenet.NetworkMessages.Login;
import net.tacospice.stevenet.NetworkMessages.UserConnect;
import net.tacospice.stevenet.NetworkMessages.UserDisconnect;
import net.tacospice.stevenet.NetworkMessages.UserList;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.KeepAlive;
import com.esotericsoftware.kryonet.Listener;

public class NetClient extends ClientServer
{
	private final Client kryoClient;
	private ClientEventHandler eventHandler = null;

	private String name;

	public NetClient()
	{
		super(false);

		kryoClient = new Client();
		NetworkMessages.registerBuiltInMessages(kryoClient);

		kryoClient.addListener(new Listener()
		{
			@Override
			public void connected(Connection connection)
			{
				Login login = new Login();
				login.name = name;
				sendTCP(login);
			}

			@Override
			public void received(Connection connection, Object object)
			{
				if (object instanceof Login)
				{
					localConnected(((Login) object).name);
					return;
				}

				if (object instanceof Disconnect)
				{
					localDisconnected(((Disconnect) object).reason);
					return;
				}

				// Connect, disconnect
				if (object instanceof UserConnect)
				{
					userConnected(((UserConnect) object).name);
					return;
				}

				if (object instanceof UserDisconnect)
				{
					userDisconnected(((UserDisconnect) object).name);
					return;
				}

				// Userlist updated
				if (object instanceof UserList)
				{
					// UserList userList = (UserList) object;

					return;
				}

				// Chat message
				if (object instanceof ChatMessage)
				{
					ChatMessage message = (ChatMessage) object;
					logMessage("> " + message.text);

					return;
				}

				// Node created
				if (object instanceof CreateNode)
				{
					CreateNode cn = (CreateNode) object;

					nodeCreated(cn.id, cn.classId, cn.owner);
					return;
				}

				// Node destroyed
				if (object instanceof DestroyNode)
				{
					DestroyNode cn = (DestroyNode) object;

					removeNode(nodes.get(cn.id));
					nodeDestroyed(cn.id);
					return;
				}

				// Replicator update
				if (object instanceof ReplicatorMessage)
				{
					int nodeId = ((ReplicatorMessage) object).nodeId;
					int repId = ((ReplicatorMessage) object).replicatorId;

					Node n = nodes.get(nodeId);

					if (n != null)
					{
						Replicator r = n.getReplicator(repId);

						if (r != null)
							r.updateFromMessage((ReplicatorMessage) object);
					}

					return;
				}

				// Don't consider keepalive an unknown message
				if (object instanceof KeepAlive)
					return;

				unknownMessage(object);
			}

			@Override
			public void disconnected(Connection connection)
			{

				close();
			}
		});
	}

	public void connect(String name, String host, int tcpPort, int udpPort, int timeout) throws IOException
	{
		this.name = name;

		kryoClient.start();
		kryoClient.connect(timeout, host, tcpPort, udpPort);
	}

	public void close()
	{
		kryoClient.close();
		kryoClient.stop();
		clearNodes();
	}

	public Client getKryoClient()
	{
		return kryoClient;
	}

	public void sendChat(String message)
	{
		if (message == null || message.length() == 0)
			return;

		ChatMessage chatMessage = new ChatMessage();
		chatMessage.text = message;

		sendTCP(chatMessage);
	}

	public void setEventHandler(ClientEventHandler handler)
	{
		eventHandler = handler;
	}

	private void logMessage(String message)
	{
		if (eventHandler != null)
			eventHandler.logMessage(message);
	}

	private void nodeCreated(int nodeId, int classId, boolean owner)
	{
		if (eventHandler != null)
			eventHandler.nodeCreated(nodeId, classId, owner);
	}

	private void nodeDestroyed(int nodeId)
	{
		if (eventHandler != null)
			eventHandler.nodeDestroyed(nodeId);
	}

	private void userConnected(String name)
	{
		if (eventHandler != null)
			eventHandler.userConnected(name);
	}

	private void userDisconnected(String name)
	{
		if (eventHandler != null)
			eventHandler.userDisconnected(name);
	}

	public void localConnected(String name)
	{
		if (eventHandler != null)
			eventHandler.localConnected(name);
	}

	public void localDisconnected(NetworkMessages.Disconnect.Reason reason)
	{
		if (eventHandler != null)
			eventHandler.localDisconnected(reason);
	}

	public void connectionClosed()
	{
		if (eventHandler != null)
			eventHandler.connectionClosed();
	}

	private void unknownMessage(Object message)
	{
		if (eventHandler != null)
			eventHandler.unknownMessage(message);
	}

	/*
	 * NetEndpoint methods
	 */
	@Override
	public void sendTCP(Object message)
	{
		kryoClient.sendTCP(message);
	}

	@Override
	public void sendUDP(Object message)
	{
		kryoClient.sendUDP(message);
	}

	/*
	 * Unused methods
	 */

	@Override
	public void sendToAllTCP(Object message)
	{
	}

	@Override
	public void sendToAllExceptTCP(int id, Object message)
	{
	}

	@Override
	public void sendToTCP(int id, Object message)
	{
	}

	@Override
	public void sendToAllUDP(Object message)
	{
	}

	@Override
	public void sendToAllExceptUDP(int id, Object message)
	{
	}

	@Override
	public void sendToUDP(int id, Object message)
	{
	}
}