package net.tacospice.stevenet;

public abstract class Replicator
{
	public enum ReplicationType
	{
		ToNeither, ToProxy, ToOwner, ToBoth
	}

	private final ReplicationType replicationType;
	private final boolean ownerToServer;
	protected boolean modified;
	private final boolean alwaysUpdate;

	public Replicator(ReplicationType replicationType, boolean ownerToServer, boolean alwaysUpdate)
	{
		if (replicationType == null)
			throw new IllegalArgumentException("replicationType must be non-null");

		this.replicationType = replicationType;
		this.ownerToServer = ownerToServer;
		modified = false;
		this.alwaysUpdate = alwaysUpdate;
	}

	public ReplicationType getReplicationType()
	{
		return replicationType;
	}

	public boolean getOwnerToServer()
	{
		return ownerToServer;
	}

	public boolean alwaysUpdate()
	{
		return alwaysUpdate;
	}

	public void markSent()
	{
		modified = false;
	}

	public boolean needSend()
	{
		return modified;
	}

	abstract public void updateFromMessage(ReplicatorMessage message);

	abstract public ReplicatorMessage getMessage(int nodeId, int replicatorId);
}