package net.tacospice.stevenet;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

public class NetworkMessages
{
	static public void registerBuiltInMessages(EndPoint endpoint)
	{
		Kryo k = endpoint.getKryo();

		k.register(Login.class);

		k.register(Disconnect.class);
		k.register(Disconnect.Reason.class);

		k.register(UserConnect.class);
		k.register(UserDisconnect.class);
		k.register(ChatMessage.class);
		k.register(UserList.class);

		k.register(CreateNode.class);
		k.register(DestroyNode.class);

		k.register(FloatReplicator.Message.class);
		k.register(IntReplicator.Message.class);
		k.register(Vector2fReplicator.Message.class);
		k.register(Vector2iReplicator.Message.class);
	}

	static public class Login
	{
		String name;
	}

	static public class Disconnect
	{
		static public enum Reason
		{
			Unknown, AlreadyLoggedIn, NoName, ZeroNameLength, DidNotRegister
		};

		Reason reason;
	}

	static public class UserConnect
	{
		String name;
	}

	static public class UserDisconnect
	{
		String name;
	}

	// TODO: store username in message also
	static public class ChatMessage
	{
		String text;
	}

	static public class UserList
	{
		String[] names;
	}

	static public class CreateNode
	{
		int id;
		int classId;
		boolean owner;
	}

	static public class DestroyNode
	{
		int id;
	}
}
