package net.tacospice.stevenet;

abstract public class NetEndpoint
{
	/*
	 * TCP
	 */
	abstract public void sendToAllTCP(Object message);

	abstract public void sendToAllExceptTCP(int id, Object message);

	abstract public void sendToTCP(int id, Object message);

	abstract public void sendTCP(Object message);

	/*
	 * UDP
	 */
	abstract public void sendToAllUDP(Object message);

	abstract public void sendToAllExceptUDP(int id, Object message);

	abstract public void sendToUDP(int id, Object message);

	abstract public void sendUDP(Object message);
}
