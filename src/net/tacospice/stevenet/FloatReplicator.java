package net.tacospice.stevenet;

public final class FloatReplicator extends Replicator
{
	private float value;

	public FloatReplicator(ReplicationType replicationType, boolean ownerToServer, boolean alwaysUpdate)
	{
		super(replicationType, ownerToServer, alwaysUpdate);

		value = 0.0f;
	}

	public void setValue(float value)
	{
		if (value != this.value)
		{
			this.value = value;
			modified = true;
		}
	}

	public float getValue()
	{
		return value;
	}

	/*
	 * Replicator's network message class
	 */
	static public class Message extends ReplicatorMessage
	{
		public float value;
	}

	@Override
	public void updateFromMessage(ReplicatorMessage message)
	{
		if (message instanceof Message)
		{
			setValue(((Message) message).value);
		}
	}

	@Override
	public ReplicatorMessage getMessage(int nodeId, int replicatorId)
	{
		Message m = new Message();
		m.value = value;
		m.nodeId = nodeId;
		m.replicatorId = replicatorId;

		return m;
	}

}
