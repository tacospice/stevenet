package net.tacospice.stevenet;

public abstract class ReplicatorMessage
{
	public int packetSequence;
	public int nodeId;
	public int replicatorId;
}
