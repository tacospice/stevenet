package net.tacospice.stevenet;

public interface ServerEventHandler
{
	public void logMessage(String message);

	public void clientConnected(NetConnection connection);

	public void clientDisconnected(NetConnection connection);

	public void unknownMessage(Object message);
}