package net.tacospice.stevenet;

public final class Vector2iReplicator extends Replicator
{
	private int x, y;

	public Vector2iReplicator(ReplicationType replicationType, boolean ownerToServer, boolean alwaysUpdate)
	{
		super(replicationType, ownerToServer, alwaysUpdate);

		x = y = 0;
	}

	public void setFields(int x, int y)
	{
		if (x != this.x || y != this.y)
		{
			this.x = x;
			this.y = y;
			modified = true;
		}
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	/*
	 * Replicator's network message class
	 */
	static public class Message extends ReplicatorMessage
	{
		public int x;
		public int y;
	}

	@Override
	public void updateFromMessage(ReplicatorMessage message)
	{
		if (message instanceof Message)
			setFields(((Message) message).x, ((Message) message).y);
	}

	@Override
	public ReplicatorMessage getMessage(int nodeId, int replicatorId)
	{
		Message m = new Message();
		m.x = x;
		m.y = y;
		m.nodeId = nodeId;
		m.replicatorId = replicatorId;

		return m;
	}

}
