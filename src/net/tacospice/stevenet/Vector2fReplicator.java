package net.tacospice.stevenet;

public final class Vector2fReplicator extends Replicator
{
	private float x, y;

	public Vector2fReplicator(ReplicationType replicationType, boolean ownerToServer, boolean alwaysUpdate)
	{
		super(replicationType, ownerToServer, alwaysUpdate);

		x = y = 0.0f;
	}

	public void setFields(float x, float y)
	{
		if (x != this.x || y != this.y)
		{
			this.x = x;
			this.y = y;
			modified = true;
		}
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	/*
	 * Replicator's network message class
	 */
	static public class Message extends ReplicatorMessage
	{
		public float x;
		public float y;
	}

	@Override
	public void updateFromMessage(ReplicatorMessage message)
	{
		if (message instanceof Message)
			setFields(((Message) message).x, ((Message) message).y);
	}

	@Override
	public ReplicatorMessage getMessage(int nodeId, int replicatorId)
	{
		Message m = new Message();
		m.x = x;
		m.y = y;
		m.nodeId = nodeId;
		m.replicatorId = replicatorId;

		return m;
	}

}
